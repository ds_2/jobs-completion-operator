# Build instructions

This operator requires:

* Go v1.16+
* Operator SDK v1.10.0+

## New APIs

    operator-sdk create api --group=jobs --version=v1alpha1 --kind=JobsLookup --resource=true --controller=true
    operator-sdk create api --group=jobs --version=v1alpha1 --kind=Job --resource=false --controller=true

## Build

    operator-sdk version
    make build manifests

## Test in your cluster

### From your IDE

    kubectx YOURCLUSTERCTX
    kubens testing
    make install run

Now you can already watch the changes on the job reconciler.

### From a 2nd terminal

Install the CRD:

    kubectl apply -f config/samples/jobs_v1alpha1_jobslookup.yaml

Now you can check if the crd gets installed, the configmap set up, and if the cronjob/job fails -> trigger alert.

To uninstall the CRD, run:

    kubectl delete -f config/samples/jobs_v1alpha1_jobslookup.yaml

## Release for deploying (Release managers, only!!)

Simply apply a tag on the commit. Gitlab will build it.

The operator image will be hosted at <https://quay.io/ds2/jobs-completion-operator-bundle>.
