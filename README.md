# Jobs Completion Notifier

[![Docker Repository on Quay](https://quay.io/repository/ds2/jobs-completion-operator-bundle/status "Docker Repository on Quay")](https://quay.io/repository/ds2/jobs-completion-operator-bundle)
[![pipeline status](https://gitlab.com/ds_2/jobs-completion-operator/badges/develop/pipeline.svg)](https://gitlab.com/ds_2/jobs-completion-operator/-/commits/develop)
[![coverage report](https://gitlab.com/ds_2/jobs-completion-operator/badges/develop/coverage.svg)](https://gitlab.com/ds_2/jobs-completion-operator/-/commits/develop)

A dummy kubernetes operator to watch over some jobs and checks if they complete/timeout.

## Install/Uninstall operator from your cluster

### Preflight

Setup your environment:

    kubectx YOURCTX
    operator-sdk olm status

If the OLM (Operator Lifecycle Manager) reports an issue saying that it could not detect the OLM runtime, then perhaps you have not yet installed OLM. Run:

    operator-sdk olm install --verbose

In case you face any issues, please check if the OLM is maybe already installed. Or maybe not cleanly uninstalled. Check if the operators namespace does not exist before you try to install it.

Be aware that this is only required when you want to run the operator by itself. Usually, during development, you run the operator on your machine instead autonomous.

The OLM will install itself in the OLM namespace. For running your own operators, use the operators namespace.

### Installation

You can find the latest version of the operator via <https://gitlab.com/ds_2/jobs-completion-operator/-/releases>.

Run:

    kubens operators # use the namespace of your choice; but operators is recommended
    operator-sdk run bundle quay.io/ds2/jobs-completion-operator-bundle:v0.1.3

To update/upgrade the operator, run:

    kubens operators
    operator-sdk run bundle-upgrade quay.io/ds2/jobs-completion-operator-bundle:NEWTAGVERSION

You can now install the CRDs etc.

### Uninstall the operator

Uninstall it via:

    kubens operators
    operator-sdk cleanup jobs-completion-operator

It may take one minute for closing all open catalogs.
