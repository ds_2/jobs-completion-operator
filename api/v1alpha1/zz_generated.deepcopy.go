//go:build !ignore_autogenerated
// +build !ignore_autogenerated

/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by controller-gen. DO NOT EDIT.

package v1alpha1

import (
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *JobsLookup) DeepCopyInto(out *JobsLookup) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new JobsLookup.
func (in *JobsLookup) DeepCopy() *JobsLookup {
	if in == nil {
		return nil
	}
	out := new(JobsLookup)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *JobsLookup) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *JobsLookupList) DeepCopyInto(out *JobsLookupList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]JobsLookup, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new JobsLookupList.
func (in *JobsLookupList) DeepCopy() *JobsLookupList {
	if in == nil {
		return nil
	}
	out := new(JobsLookupList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *JobsLookupList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *JobsLookupSpec) DeepCopyInto(out *JobsLookupSpec) {
	*out = *in
	if in.Tags != nil {
		in, out := &in.Tags, &out.Tags
		*out = make([]string, len(*in))
		copy(*out, *in)
	}
	if in.CronJobs != nil {
		in, out := &in.CronJobs, &out.CronJobs
		*out = make([]string, len(*in))
		copy(*out, *in)
	}
	out.OpsGenieDetails = in.OpsGenieDetails
	out.TelegramDetails = in.TelegramDetails
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new JobsLookupSpec.
func (in *JobsLookupSpec) DeepCopy() *JobsLookupSpec {
	if in == nil {
		return nil
	}
	out := new(JobsLookupSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *JobsLookupStatus) DeepCopyInto(out *JobsLookupStatus) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new JobsLookupStatus.
func (in *JobsLookupStatus) DeepCopy() *JobsLookupStatus {
	if in == nil {
		return nil
	}
	out := new(JobsLookupStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OpsGenieDetails) DeepCopyInto(out *OpsGenieDetails) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OpsGenieDetails.
func (in *OpsGenieDetails) DeepCopy() *OpsGenieDetails {
	if in == nil {
		return nil
	}
	out := new(OpsGenieDetails)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *SlackDetails) DeepCopyInto(out *SlackDetails) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SlackDetails.
func (in *SlackDetails) DeepCopy() *SlackDetails {
	if in == nil {
		return nil
	}
	out := new(SlackDetails)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *TelegramDetails) DeepCopyInto(out *TelegramDetails) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new TelegramDetails.
func (in *TelegramDetails) DeepCopy() *TelegramDetails {
	if in == nil {
		return nil
	}
	out := new(TelegramDetails)
	in.DeepCopyInto(out)
	return out
}
