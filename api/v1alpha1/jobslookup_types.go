/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type OpsGenieDetails struct {
	//defines the secret name where to look for the credentials
	SecretName string `json:"secretName,omitempty"`
	//defines the priority for the alert: P1=high, P5=low; defaults to P5
	Priority string `json:"priority,omitempty"`
	ApiUrl   string `json:"apiUrl,omitempty"`
}

type TelegramDetails struct {
	SecretName string `json:"secretName,omitempty"`
}

type SlackDetails struct {
	//secretName should contain the webhook url to use
	SecretName string `json:"secretName,omitempty"`
}

// JobsLookupSpec defines the desired state of JobsLookup
type JobsLookupSpec struct {
	//The tags to apply to the alerts
	Tags []string `json:"tags,omitempty"`
	//Instead of monitoring one cronjobs, monitor these cronjobs
	CronJobs []string `json:"cronJobs,omitempty"`
	//flag to monitor all the cronjobs in this namespace
	AllCronjobsInNamespace   bool  `json:"allCronjobsInNamespace,omitempty"`
	CloseOpenAlertsOnSuccess bool  `json:"closeOpenAlertsOnSuccess,omitempty"`
	InitialIgnoreCount       uint8 `json:"initialIgnoreCount,omitempty"`
	//defines the secret name for looking up the opsgenie data; defaults to opsgenie
	OpsGenieDetails OpsGenieDetails `json:"opsGenieDetails,omitempty"`
	TelegramDetails TelegramDetails `json:"telegramDetails,omitempty"`
}

// JobsLookupStatus defines the observed state of JobsLookup
type JobsLookupStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// JobsLookup is the Schema for the jobslookups API
type JobsLookup struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   JobsLookupSpec   `json:"spec,omitempty"`
	Status JobsLookupStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// JobsLookupList contains a list of JobsLookup
type JobsLookupList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []JobsLookup `json:"items"`
}

func init() {
	SchemeBuilder.Register(&JobsLookup{}, &JobsLookupList{})
}
