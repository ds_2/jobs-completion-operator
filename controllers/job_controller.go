/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"strconv"
	"time"

	"github.com/go-logr/logr"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

const (
	SecApiKey   = "apiKey"
	SecTeamName = "teamName"
)

// JobReconciler reconciles a Job object
type JobReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch
//+kubebuilder:rbac:groups=jobs.ds-2.de,resources=jobs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=jobs.ds-2.de,resources=jobs/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=jobs.ds-2.de,resources=jobs/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Job object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.8.3/pkg/reconcile
func (r *JobReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	logger.Info("A job has changed its state. Checking metadata..")
	// get cronjob name from job
	jobDetails := &batchv1.Job{}
	err := r.Get(ctx, req.NamespacedName, jobDetails)
	if err != nil {
		if errors.IsNotFound(err) {
			logger.Info("Job not found. Perhaps this was a deletion event. Ignoring.", "JobDetails", req.NamespacedName)
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		logger.Error(err, "Failed to get job details.")
		return ctrl.Result{}, err
	}
	r.handleJob(logger, jobDetails)

	return ctrl.Result{}, nil
}

func (r *JobReconciler) handleJob(logger logr.Logger, details *batchv1.Job) {
	jobOwners := details.OwnerReferences
	if len(jobOwners) <= 0 {
		//logger.Info("Job has no owner??")
		return
	}
	//logger.Info("Testing owner..", "Owners", jobOwners)
	if jobOwners[0].Kind != "CronJob" {
		//logger.Info("This job is not controlled by a cronjob! Ignoring.", "Owner", jobOwners[0])
		return
	}
	cronJobName := jobOwners[0].Name
	jobStatus := details.Status
	//logger.Info("Job data here:", "Status", jobStatus, "CronJob", cronJobName)
	succeededCount := jobStatus.Succeeded
	completionTime := jobStatus.CompletionTime
	failedCount := jobStatus.Failed
	if completionTime == nil && failedCount <= 0 {
		logger.Info("Job may just have been started due to no completionTime. Ignoring.", "failed", failedCount, "succ", succeededCount)
		return
	}
	foundConfig, cmError := r.loadConfigmapForCronjob(details.Namespace, cronJobName)
	if errors.IsNotFound(cmError) {
		foundConfig, cmError = r.loadGlobalConfigmap(details.Namespace)
	}
	if cmError != nil {
		logger.Info("no configmap found for this cronjob. Ignoring.", "Name", cronJobName)
		return
	}
	opsGenSecretName := foundConfig.Data[CmOpsgensecret]
	opsGenPrio := foundConfig.Data[CmOpsGenPrio]
	opsGenTags := foundConfig.Data[CmTags]
	ignoreCount := toInt(foundConfig.Data[CmIgnoreCount], 0)
	closeAlertsOnSuccess := toBool(foundConfig.Data[CmCloseAlertsOnSuccess], false)
	opsGenUrl := foundConfig.Data[CmOpsGenApiUrl]
	opsGenSec, secErr := r.loadSecretByName(details.Namespace, opsGenSecretName)
	if succeededCount <= 0 {
		logger.Info("The given job has failed!", "cronName", cronJobName, "completed", completionTime)
		if secErr == nil {
			logger.Info("Will trigger opsGen alert now..")
			handleJobError(cronJobName, jobStatus, opsGenSec, opsGenPrio, opsGenTags, details, ignoreCount, opsGenUrl)
		} else {
			logger.Info("Could not find secret from ConfigMap. Ignoring report! Maybe this is not good ;)", "cmName", cronJobName, "secretName", opsGenSecretName)
		}
	} else if succeededCount > 0 {
		logger.Info("The job ran successful:", "cronName", cronJobName, "completed", completionTime)
		if closeAlertsOnSuccess {
			handleJobSuccess(cronJobName, jobStatus, opsGenSec, opsGenPrio, opsGenTags, details, opsGenUrl)
		}
	}
}

func toBool(s string, defVal bool) bool {
	parseBool, err := strconv.ParseBool(s)
	if err != nil {
		return defVal
	}
	return parseBool
}

func toInt(s string, def int) int {
	val, err := strconv.Atoi(s)
	if err != nil {
		return def
	}
	return val
}

func handleJobSuccess(cronJobName string, jobStatus batchv1.JobStatus, opsGenSecret v1.Secret, prio string, tags string, jobData *batchv1.Job, opsGenApiUrl string) {
	go HandleOpsGenieSuccessRun(cronJobName, opsGenSecret, tags, jobData, opsGenApiUrl)
}

func handleJobError(cronJobName string, jobStatus batchv1.JobStatus, opsGenSecret v1.Secret, prio string, tags string, jobData *batchv1.Job, ignoreCount int, opsGenApiUrl string) {
	go TriggerOpsGenieAlert(cronJobName, jobStatus, jobData.Name, opsGenSecret, prio, tags, jobData, ignoreCount, opsGenApiUrl)
}

func getJobDuration(details *batchv1.Job) time.Duration {
	startTime := details.CreationTimestamp
	completeTime := details.Status.CompletionTime
	if completeTime != nil {
		return completeTime.Sub(startTime.Time)
	}
	return time.Now().Sub(startTime.Time)
}

func (r *JobReconciler) loadConfigmapForCronjob(namespace string, cronJobName string) (v1.ConfigMap, error) {
	foundConfigMap := v1.ConfigMap{}
	err := r.Get(context.TODO(), types.NamespacedName{Namespace: namespace, Name: cronJobName + SingleConfigmapPostfix}, &foundConfigMap)
	return foundConfigMap, err
}
func (r *JobReconciler) loadSecretByName(namespace string, secretName string) (v1.Secret, error) {
	foundSecret := v1.Secret{}
	err := r.Get(context.TODO(), types.NamespacedName{Namespace: namespace, Name: secretName}, &foundSecret)
	return foundSecret, err
}

// SetupWithManager sets up the controller with the Manager.
func (r *JobReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&batchv1.Job{}).
		Complete(r)
}

//tries to load the global configMap
func (r *JobReconciler) loadGlobalConfigmap(namespace string) (v1.ConfigMap, error) {
	foundConfigMap := v1.ConfigMap{}
	err := r.Get(context.TODO(), types.NamespacedName{Namespace: namespace, Name: GlobalNsConfigmapName}, &foundConfigMap)
	return foundConfigMap, err
}
