/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	client2 "github.com/opsgenie/opsgenie-go-sdk-v2/client"
	jobsv1alpha1 "gitlab.com/ds_2/jobs-completion-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

const (
	CmCronjob                 = "cronJobName"
	CmNs                      = "namespace"
	CmOpsgensecret            = "opsGenSecret"
	CmOpsGenPrio              = "opsGenPriority"
	CmTags                    = "tags"
	CmIgnoreCount             = "ignoreCount"
	CmCloseAlertsOnSuccess    = "closeAlertsWhenSuccessful"
	CmOpsGenApiUrl            = "opsGenApiUrl"
	GlobalNsConfigmapName     = "jobs-monitor-namespaced-config"
	OpsGenieDefaultSecretName = "opsgenie"
	OpsGenieDefaultPriority   = "P5"
	DefaultIgnoreCount        = "0"
	AlertDefaultTags          = "kubernetes,cronjob"
	SingleConfigmapPostfix    = "-config"
)

// JobsLookupReconciler reconciles a JobsLookup object
type JobsLookupReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=jobs.ds-2.de,resources=jobslookups,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=jobs.ds-2.de,resources=jobslookups/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the JobsLookup object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.8.3/pkg/reconcile
func (r *JobsLookupReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx).WithValues("name", req.NamespacedName)
	dto := &jobsv1alpha1.JobsLookup{}
	err := r.Get(ctx, req.NamespacedName, dto)
	if err != nil {
		if errors.IsNotFound(err) {
			logger.Info("Event is a delete event. Ignoring.")
			return ctrl.Result{}, nil
		}
		logger.Error(err, "Failed to get dto details. Sending error to master controller.")
		return ctrl.Result{}, err
	}
	logger.Info("Spec installed", "Spec", dto.Spec)
	if dto.Spec.AllCronjobsInNamespace {
		r.deleteSingleConfig(dto)
		globalConfigMapFromSpec := createConfigMapBody(*dto, true)
		err := r.updateOrCreateConfigmap(dto, globalConfigMapFromSpec)
		if err != nil {
			return ctrl.Result{}, err
		}
	} else {
		r.deleteGlobalNamespacedConfig(dto)
		//single scoped
		configMapFromSpec := createConfigMapBody(*dto, false)
		err := r.updateOrCreateConfigmap(dto, configMapFromSpec)
		if err != nil {
			return ctrl.Result{}, err
		}
	}
	logger.Info("Done with checking the spec!")
	return ctrl.Result{}, nil
}

func (r *JobsLookupReconciler) updateOrCreateConfigmap(dto *jobsv1alpha1.JobsLookup, configMapFromSpec *corev1.ConfigMap) error {
	if err := controllerutil.SetControllerReference(dto, configMapFromSpec, r.Scheme); err != nil {
		return err
	}
	foundConfigMap := corev1.ConfigMap{}
	err2 := r.Get(context.TODO(), types.NamespacedName{Name: configMapFromSpec.Name, Namespace: configMapFromSpec.Namespace}, &foundConfigMap)
	if err2 != nil && errors.IsNotFound(err2) {
		fmt.Println("Creating CM", configMapFromSpec.Name, "..")
		err2 = r.Create(context.TODO(), configMapFromSpec)
		if err2 != nil {
			return err2
		}
		return nil
	} else if err2 != nil {
		return err2
	}
	if configMapIsDifferent(foundConfigMap, configMapFromSpec) {
		fmt.Println("Updating CM :)", configMapFromSpec.Name)
		err2 = r.Update(context.TODO(), configMapFromSpec)
		if err2 != nil {
			return err2
		}
	}
	return nil
}

func createConfigMapBody(cr jobsv1alpha1.JobsLookup, globalCm bool) *corev1.ConfigMap {
	labels := map[string]string{
		"app": cr.Name,
	}
	configMapName := cr.Name + SingleConfigmapPostfix
	if globalCm {
		configMapName = GlobalNsConfigmapName
	}
	myConfigMap := corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      configMapName,
			Namespace: cr.Namespace,
			Labels:    labels,
		},
		Data: map[string]string{
			CmCloseAlertsOnSuccess: strconv.FormatBool(cr.Spec.CloseOpenAlertsOnSuccess),
			CmIgnoreCount:          strconv.Itoa(int(cr.Spec.InitialIgnoreCount)),
			CmTags:                 strings.Join(cr.Spec.Tags, ","),
		},
	}
	opsGenDetails := cr.Spec.OpsGenieDetails
	myConfigMap.Data[CmOpsgensecret] = opsGenDetails.SecretName
	myConfigMap.Data[CmOpsGenPrio] = opsGenDetails.Priority
	myConfigMap.Data[CmOpsGenApiUrl] = opsGenDetails.ApiUrl
	//some default values in case the user did not enter them
	if len(myConfigMap.Data[CmTags]) <= 0 {
		myConfigMap.Data[CmTags] = AlertDefaultTags
	}
	if len(myConfigMap.Data[CmOpsGenApiUrl]) <= 0 {
		myConfigMap.Data[CmOpsGenApiUrl] = string(client2.API_URL)
	}
	if len(myConfigMap.Data[CmOpsGenPrio]) <= 0 {
		myConfigMap.Data[CmOpsGenPrio] = OpsGenieDefaultPriority
	}
	if len(myConfigMap.Data[CmOpsgensecret]) <= 0 {
		myConfigMap.Data[CmOpsgensecret] = OpsGenieDefaultSecretName
	}
	if len(myConfigMap.Data[CmIgnoreCount]) <= 0 {
		myConfigMap.Data[CmIgnoreCount] = DefaultIgnoreCount
	}
	return &myConfigMap
}

func configMapIsDifferent(originalCm corev1.ConfigMap, newCm *corev1.ConfigMap) bool {
	if len(originalCm.Data) != len(newCm.Data) {
		return true
	}
	if originalCm.Data[CmCronjob] != newCm.Data[CmCronjob] {
		return true
	}
	if originalCm.Data[CmNs] != newCm.Data[CmNs] {
		return true
	}
	if originalCm.Data[CmOpsgensecret] != newCm.Data[CmOpsgensecret] {
		return true
	}
	if originalCm.Data[CmTags] != newCm.Data[CmTags] {
		return true
	}
	if originalCm.Data[CmOpsGenPrio] != newCm.Data[CmOpsGenPrio] {
		return true
	}
	if originalCm.Data[CmOpsGenApiUrl] != newCm.Data[CmOpsGenApiUrl] {
		return true
	}
	if originalCm.Data[CmIgnoreCount] != newCm.Data[CmIgnoreCount] {
		return true
	}
	if originalCm.Data[CmCloseAlertsOnSuccess] != newCm.Data[CmCloseAlertsOnSuccess] {
		return true
	}
	return false
}

// SetupWithManager sets up the controller with the Manager.
func (r *JobsLookupReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&jobsv1alpha1.JobsLookup{}).
		WithOptions(controller.Options{
			MaxConcurrentReconciles: 1,
		}).
		Complete(r)
}

func (r *JobsLookupReconciler) deleteGlobalNamespacedConfig(dto *jobsv1alpha1.JobsLookup) {
	foundGlobalConfig := corev1.ConfigMap{}
	err := r.Get(context.TODO(), types.NamespacedName{
		Namespace: dto.Namespace,
		Name:      GlobalNsConfigmapName,
	}, &foundGlobalConfig)
	if err != nil {
		return
	}
	err = r.Delete(context.TODO(), &foundGlobalConfig)
	if err != nil {
		return
	}
}

func (r *JobsLookupReconciler) deleteSingleConfig(dto *jobsv1alpha1.JobsLookup) {
	thisConfigMap := corev1.ConfigMap{}
	err := r.Get(context.TODO(), types.NamespacedName{
		Namespace: dto.Namespace,
		Name:      dto.Name + SingleConfigmapPostfix,
	}, &thisConfigMap)
	if err != nil {
		fmt.Println("Could not find single config. Ignoring deletion.")
		return
	}
	err = r.Delete(context.TODO(), &thisConfigMap)
	if err != nil {
		fmt.Println("Could not delete configmap:", err)
		return
	}
}
