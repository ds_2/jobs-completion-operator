package controllers

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strings"

	alertsv2 "github.com/opsgenie/opsgenie-go-sdk-v2/alert"
	ogcli "github.com/opsgenie/opsgenie-go-sdk-v2/client"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
)

func TriggerOpsGenieAlert(cronJobName string, jobStatus batchv1.JobStatus, jobName string, opsGenSecret v1.Secret, prio string, tags string, jobData *batchv1.Job, ignoreCount int, apiUrl string) {
	fmt.Println("OK, starting opsGen setup..")
	thisApiKey := string(opsGenSecret.Data[SecApiKey])
	thisTeamName := string(opsGenSecret.Data[SecTeamName])
	alertCli, _ := alertsv2.NewClient(createOpsGenieConfig(thisApiKey, apiUrl))
	currAlertCount := getCountOfAlert(alertCli, cronJobName, jobData.Namespace)

	tagsArray := strings.Split(tags, ",")
	myTagsArray := []string{"kubernetes", "cronjob", cronJobName}
	conditionsMsg := ""
	jobFailReason := ""
	for _, condition := range jobStatus.Conditions {
		condMessage := condition.Message
		condStatus := condition.Status
		condType := condition.Type
		jobFailReason = condition.Reason
		conditionsMsg += "condition info:\nmessage: " + condMessage
		conditionsMsg += "\nstatus: " + string(condStatus)
		conditionsMsg += "\ntype: " + string(condType)
		conditionsMsg += "\nreason: " + jobFailReason
		conditionsMsg += "\nduration: " + getJobDuration(jobData).String()
		conditionsMsg += "\nstarted: " + jobData.CreationTimestamp.Time.String()
	}
	var ourResponders []alertsv2.Responder
	if currAlertCount > ignoreCount {
		fmt.Println("Adding responders..")
		ourResponders = []alertsv2.Responder{
			{Type: alertsv2.EscalationResponder, Name: thisTeamName},
		}
	} else {
		fmt.Println("Creating empty alert..")
	}
	request := alertsv2.CreateAlertRequest{
		Message:     "CronJob failed: " + cronJobName + " -> " + jobName,
		Alias:       createAlertAlias(cronJobName, jobData.Namespace),
		Description: "The given job just failed. Maybe it will autorecover on the next run.\n\nBut maybe you want to check the log files?",
		Responders:  ourResponders,
		Tags:        append(myTagsArray, tagsArray[:]...),
		Details: map[string]string{
			"cronJobName": cronJobName,
			"reason":      jobFailReason,
			"namespace":   jobData.Namespace,
		},
		Priority: mapToOpsGenPrio(prio),
		Note:     conditionsMsg,
	}

	response, err := alertCli.Create(nil, &request)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("Created request ID: " + response.RequestId)
	}
}

func createAlertAlias(cronJobName string, namespace string) string {
	return asSha256("job_" + cronJobName + "_ns_" + namespace)
}

func mapToOpsGenPrio(prio string) alertsv2.Priority {
	switch prio {
	case "P1":
		return alertsv2.P1
	case "P2":
		return alertsv2.P2
	case "P3":
		return alertsv2.P3
	case "P4":
		return alertsv2.P4
	default:
		return alertsv2.P5
	}
}

func getCountOfAlert(alertCli *alertsv2.Client, cronJobName string, namespace string) int {
	listAlertsReq := alertsv2.ListAlertRequest{
		Limit: 10,
		Sort:  "createdAt",
		Query: "alias:" + createAlertAlias(cronJobName, namespace) + " and status:open",
	}
	listAlertResult, err := alertCli.List(nil, &listAlertsReq)
	if err != nil {
		fmt.Println("Error when listing the existing alerts:", err)
		return 0
	}
	if len(listAlertResult.Alerts) > 0 {
		thisAlert := listAlertResult.Alerts[0]
		return thisAlert.Count
	}
	return 0
}

func asSha256(s string) string {
	hash := sha256.Sum256([]byte(s))
	return hex.EncodeToString(hash[:])
}

func HandleOpsGenieSuccessRun(cronJobName string, opsGenSecret v1.Secret, tags string, jobData *batchv1.Job, apiUrl string) {
	thisApiKey := string(opsGenSecret.Data[SecApiKey])
	thisTeamName := string(opsGenSecret.Data[SecTeamName])
	fmt.Println("Data so far: ", thisApiKey, " and team=", thisTeamName)
	alertCli, _ := alertsv2.NewClient(createOpsGenieConfig(thisApiKey, apiUrl))

	listAlertsReq := alertsv2.ListAlertRequest{
		Limit: 10,
		Sort:  "createdAt",
		Query: "alias:" + createAlertAlias(cronJobName, jobData.Namespace) + " and status:open",
	}
	listAlertResult, err := alertCli.List(nil, &listAlertsReq)
	if err != nil {
		fmt.Println("Error when listing the existing alerts:", err)
		return
	}
	if len(listAlertResult.Alerts) > 0 {
		thisAlertToClose := listAlertResult.Alerts[0]
		closeReq := alertsv2.CloseAlertRequest{
			IdentifierType:  alertsv2.ALERTID,
			IdentifierValue: thisAlertToClose.Id,
			Note:            "Closing this alert because now it ran successfully! Thanks :)",
		}
		alertResult, err := alertCli.Close(nil, &closeReq)
		if err != nil {
			fmt.Println("Error when closing the existing alert:", thisAlertToClose, err)
			return
		}
		fmt.Println("Done with closing this alert:", alertResult.Result, thisAlertToClose)
	}
}

func createOpsGenieConfig(apiKey string, apiUrl string) *ogcli.Config {
	opsGenApiUrl := ogcli.API_URL
	if strings.Contains(apiUrl, string(ogcli.API_URL_EU)) {
		opsGenApiUrl = ogcli.API_URL_EU
	}
	return &ogcli.Config{
		ApiKey:         apiKey,
		OpsGenieAPIURL: opsGenApiUrl,
		//Backoff: func(min, max time.Duration, attemptNum int, resp *http.Response) time.Duration {
		//	mult := math.Pow(2, float64(attemptNum)) * float64(min)
		//	sleep := time.Duration(mult)
		//	if float64(sleep) != mult || sleep > max {
		//		sleep = max
		//	}
		//	return sleep
		//},
	}
}
