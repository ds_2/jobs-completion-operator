# Configuration

The operator is configured via configmaps and secrets, and its own CRD.

## The CRD

The crd contains the basic configuration to configure the operator:

```
apiVersion: jobs.ds-2.de/v1alpha1
kind: JobsLookup
metadata:
  name: my-config-1
  namespace: my-cronjobs-namespace
spec:
  allCronjobsInNamespace: true # if this config applies to all cronjobs in this namespace
  tags:
    - tag1
    - test1
  opsGenieSecret: "opsgen-secret"
  opsGeniePriority: "P4"
```

This tells the operator to use the OpsGenie secret "opsgen-secret" for all the cronjobs in
namespace my-cronjobs-namespace.

## The OpsGenie secret

The pattern is as follows:

```
apiVersion: v1
kind: Secret
metadata:
  name: opsgen-secret
  namespace: my-cronjobs-namespace
type: Opaque
data:
  # echo -n 'MYPW' | base64
  teamName: YOURTEAMNAME
  alertUrl: YOURAPIURL
  apiKey: YOURAPIKEY
```

Be aware that these values have to be base64 encoded.
