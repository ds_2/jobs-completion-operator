# Changelog

## v0.2.0

### Breaking changes

* the configuration for OpsGenie and any other notifications is now a separate object (was a property before); see the example in the testing-stuff area (or the openapi spec)

### Features

* the operator will now tell via OpsGenie Alerts the kind of fail: DeadlineExceeded etc.

## v0.1.0

Initial release for public.

### Features

* monitors job completions
* reports errors via OpsGenie alerts (if configured!)
